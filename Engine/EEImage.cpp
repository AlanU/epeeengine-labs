/*
*  EEImage.cpp
*
*
*  Epee Engine
*  Created by Alan Uthoff on 3/12/06.
Copyright (C) 2011

This Code is free software; you can redistribute it and/or
modify it under the terms of the zlib/libpng License as published
by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This software is provided 'as-is', without any express or implied warranty.

In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.


*/

#include "EEImage.h"
#include "EpeeEngineCommenIncludes.h"
#include "EELog.h"
//**************************Begin of image Class Funtions****************************
image::image (const std::string & _FileNameImage, const std::string & _NameImageHere,int _x,int _y,int _z,bool _Transparency):EEWidget( _NameImageHere,Type_Image ,_x, _y, _z)
{
	m_sFileName=_FileNameImage;
	m_bClip=false;
	m_bTransparency=_Transparency;
	ClipingRect.x=0;
	ClipingRect.y=0;
	ClipingRect.h=0;
	ClipingRect.w=0;

}

void image::LoadTexture()
{
	EEWidget::LoadTexture();
}

std::string image::GetFileName()
{
	return m_sFileName;
}

void image::SetClippingRect(int _x,int _y,int _w,int _h)
{
	ClipingRect.x=_x;
	ClipingRect.y=_y;
	ClipingRect.w=_w;
	ClipingRect.h=_h;
	m_bClip=true;
}

bool image::GetClip()
{
	return m_bClip;
}

int image::GetClippingX()
{
	return ClipingRect.x;
}

int image::GetClippingY()
{
	return ClipingRect.y;
}

int image::GetClippingW()
{
	return ClipingRect.w;
}

int image::GetClippingH()
{
	return ClipingRect.h;
}

void image::SetTransparency (bool _Transparency)
{
	m_bTransparency=_Transparency;
}

bool image::GetTransparency()
{
	return m_bTransparency;
}


SDL_Surface * image::LoadSurface()
{
	if(m_sFileName==" ")
	{
		EE_ERROR<<"No file name associated with "<<GetName()<<std::endl;
		SetBlit(false);
		return NULL;
	}

	if(m_porignalSurface)
	{
		SDL_FreeSurface(m_porignalSurface);
	}

	m_porignalSurface=IMG_Load(m_sFileName.c_str());
	EE_DEBUG<<"Loading Image File Name "<<m_sFileName.c_str()<<std::endl;

	if(m_porignalSurface)
	{
		m_cOrgialDem.h=m_porignalSurface->h;
		m_cOrgialDem.w=m_porignalSurface->w;
		m_cOrgialDem.x=0;
		m_cOrgialDem.y=0;
		m_cBox.h=m_porignalSurface->h;
		m_cBox.w=m_porignalSurface->w;
		EE_DEBUG<<"Loaded image correctly"<<std::endl;
		SetBlit(false);
		return m_porignalSurface;
	}
	else
	{
		EE_ERROR<<"Loaded image Incorrectly "<<IMG_GetError()<<std::endl;
		return NULL;
	}
}

image::image (const std::string & _FileNameImage, const std::string & _NameImageHere,int _x,int _y,int _z,int _type,bool _Transparency):EEWidget( _NameImageHere,_type ,_x, _y, _z)
{
	m_sFileName=_FileNameImage;
	m_bClip=false;
	m_bTransparency=_Transparency;
	ClipingRect.x=0;
	ClipingRect.y=0;
	ClipingRect.h=0;
	ClipingRect.w=0;

}

image::image()
{


}

image::image(const std::string & _FileNameImage)
{
	m_sFileName=_FileNameImage;

}

SDL_Rect * image::GetClippingRect()
{
	return &ClipingRect;
}

SDL_Rect * image::GetOrignalRect()
{
	return &m_cOrgialDem;
}

//**************************End of image Class Fuctions********************************

