/*
*  EEWidget.cpp
*
*
*  Epee Engine
*  Created by Alan Uthoff on 3/12/06.
Copyright (C) 2011

This Code is free software; you can redistribute it and/or
modify it under the terms of the zlib/libpng License as published
by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This software is provided 'as-is', without any express or implied warranty.

In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.


*/

#include "EEWidget.h"
#include <iostream>
#include "EELog.h"
//***********************Sprocket Class Funtions**********************************

EEWidget::EEWidget(std::string _NameMain,int _TypeMain)
{
	m_bCurrentlySelected=false;
	m_sName	= _NameMain;
	m_iSprocketType=_TypeMain;
	m_bDraw=true;
	m_iz=0;
	m_bDraw=true;
	m_iCurrentPostion=0;
	m_iRotation=0;
	m_iScaleX=1;
	m_iScaleY=1;
	m_iAntiAliased=1;
	m_porignalSurface=NULL;
	m_bblit=true;
	if(_TypeMain==Type_AnimationScript)
	{
		m_bactive=false;
		m_cBox.w=1;
		m_cBox.h=1;
	}
	else
	{
		m_bactive=true;
	}

	m_bDontMoveButton=true;
	m_pModifySurface=NULL;
	m_bShowSurfaceOutline=false;
	m_GLTexture=0;
	m_iTextureH=0;
	m_iTextureW=0;
}

EEWidget::EEWidget(std::string _NameMain,int _TypeMain, int _x, int _y,int _z)
{
	m_bCurrentlySelected=false;
	m_sName	= _NameMain;
	m_iSprocketType=_TypeMain;
	m_cBox.x=_x;
	m_cBox.y=_y;
	m_iz=_z;
	m_bDraw=true;
	m_iCurrentPostion=0;
	m_iRotation=0;
	m_iScaleX=1;
	m_iScaleY=1;
	m_iAntiAliased=1;
	m_porignalSurface=NULL;
	m_bblit=true;
	if(_TypeMain==Type_AnimationScript)
	{
		m_bactive=false;
		m_cBox.w=1;
		m_cBox.h=1;
	}
	else
	{
		m_bactive=true;
	}
	m_bDontMoveButton=true;
	m_pModifySurface=NULL;
	m_bShowSurfaceOutline=false;
	m_GLTexture=0;
	m_iTextureH=0;
	m_iTextureW=0;
}

EEWidget::EEWidget()
{
	m_bCurrentlySelected=false;
	m_sName=" ";
	m_iSprocketType=Type_Image;
	m_GLTexture=0;
}

void EEWidget::Init()
{


}
EEWidget::~EEWidget()
{
	if(m_porignalSurface)
		SDL_FreeSurface(m_porignalSurface);

	if(m_pModifySurface)
	{
		SDL_FreeSurface(m_pModifySurface);
	}
	if(m_GLTexture!=0)
	{
		glDeleteTextures( 1, &m_GLTexture);
	}
}
bool EEWidget::WasLastSprocketClicked()
{
	return m_bCurrentlySelected;
}


void EEWidget::SetLocation(int _x,int _y)
{
	m_cBox.x=_x;
	m_cBox.y=_y;
}


int EEWidget::GetLocationX()
{
	return m_cBox.x;
}

int EEWidget::GetLocationY()
{
	return m_cBox.y;
}
std::string EEWidget::GetName()
{
	return m_sName;
}
int EEWidget::GetLocationZ ()
{
	return m_iz;
}

void EEWidget::SetHeightWidth(unsigned int _Height,unsigned int _Width)
{
	m_cBox.h=_Height;
	m_cBox.w=_Width;

}

int  EEWidget::NearestPowerOfTwo(int i)
{
	int PowerOfTwoNumber=1;
	while(PowerOfTwoNumber<i)
	{
		PowerOfTwoNumber<<=1;
	}
	return PowerOfTwoNumber;
}


void  EEWidget::SDL_GL_SurfaceToTexture(SDL_Surface * surface)
{

	//need to refine this
	if(m_GLTexture!=0)
	{
		glDeleteTextures( 1, &m_GLTexture);
		m_iTextureH=0;
		m_iTextureW=0;
		m_GLTexture=0;

	}
	int h=NearestPowerOfTwo(surface->h);
	int w=NearestPowerOfTwo(surface->w);
	Uint32 rmask, gmask, bmask, amask;
	SDL_Surface * temp = NULL;
	SDL_Surface * tempalpha = NULL;


#if SDL_BYTEORDER == SDL_BIG_ENDIAN
	rmask = 0xff000000;
	gmask = 0x00ff0000;
	bmask = 0x0000ff00;
	amask = 0x000000ff;
#else
	rmask = 0x000000ff;
	gmask = 0x0000ff00;
	bmask = 0x00ff0000;
	amask = 0xff000000;
#endif
	tempalpha = SDL_DisplayFormatAlpha(surface);
	SDL_SetAlpha(surface, 0, SDL_ALPHA_TRANSPARENT);
	temp = SDL_CreateRGBSurface(SDL_SWSURFACE, w, h,
		32, bmask, gmask, rmask, amask);
	//	SDL_BlitSurface(tempalpha, NULL, temp, NULL);
	if(m_pModifySurface)
	{
		SDL_FreeSurface(m_pModifySurface);
		m_pModifySurface=NULL;
	}


	//m_pModifySurface=rotozoomSurfaceXY(	tempalpha,270,1,-1,0);
	//SDL_FillRect(temp,NULL,SDL_MapRGBA(temp->format,255,0,0,255));
	CreateTexture(temp,tempalpha);

	//SDL_UnlockSurface(temp);
	SDL_FreeSurface(temp);

	SDL_FreeSurface(tempalpha);
	//return texture;
}

void EEWidget::CreateTexture(SDL_Surface * _baseSurface,SDL_Surface * _image)
{
	int h=NearestPowerOfTwo(_image->h);
	int w=NearestPowerOfTwo(_image->w);
	if(_baseSurface && _image)
	{
		glGenTextures(1, &m_GLTexture);
		glBindTexture(GL_TEXTURE_2D, m_GLTexture);
		//SDL_LockSurface(temp);


		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,  GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_R,GL_CLAMP);
		glTexImage2D(GL_TEXTURE_2D, 0, 4, w, h, 0, GL_BGRA, GL_UNSIGNED_BYTE, _baseSurface->pixels);
		glTexSubImage2D(GL_TEXTURE_2D,0,0,0,_image->w,_image->h,GL_BGRA,GL_UNSIGNED_BYTE,_image->pixels);
		m_iTextureH=h;
		m_iTextureW=w;
	}
	return;
}

unsigned int EEWidget::GetHeight()
{
	return m_cBox.h;
}
unsigned int EEWidget::GetWidth ()
{
	return m_cBox.w;
}

bool EEWidget::SetDraw(bool _Draw)
{
	m_bDraw=_Draw;
	if(m_bDraw==_Draw)
		return true;
	else
		return false;
}

bool EEWidget::GetDraw()
{
	return m_bDraw;
}

int EEWidget::GetType()
{
	return m_iSprocketType;
}


void EEWidget::UnLoadTexture()
{

	if(m_GLTexture!=0)
	{
		glDeleteTextures( 1, &m_GLTexture);
		m_GLTexture=0;
	}


}
void EEWidget::UnloadSurface()
{
	if(m_porignalSurface)
	{
		SDL_FreeSurface(m_porignalSurface);
		m_porignalSurface=NULL;
	}

	if(m_pModifySurface)
	{
		SDL_FreeSurface(m_pModifySurface);
		m_pModifySurface=NULL;
	}
}

bool EEWidget::IsLoaded()
{

	if(m_GLTexture)
	{
		return true;
	}
	return false;
}


SDL_Surface * EEWidget::LoadSurface()
{

	return NULL;
}
void EEWidget::LoadTexture()
{
	if(m_porignalSurface)
	{

		SDL_GL_SurfaceToTexture(m_porignalSurface);
		if(m_porignalSurface)
		{
			SDL_FreeSurface(m_porignalSurface);
			m_porignalSurface=NULL;
		}

		if(m_pModifySurface)
		{
			SDL_FreeSurface(m_pModifySurface);
			m_pModifySurface=NULL;
		}

	}
}

void EEWidget::Render()
{
	//not fliped

	/*if(m_GLTexture!=0)
	{
	// glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_REPLACE);
	glBindTexture( GL_TEXTURE_2D, m_GLTexture );

	glBegin( GL_QUADS );
	// Top-left vertex (corner)
	glTexCoord2i( 0, 0 );
	glVertex2f( m_cBox.x, m_cBox.y);


	// Bottom-left vertex (corner)
	glTexCoord2i( 1, 0 );
	glVertex2f( m_cBox.x, m_cBox.y+m_cBox.h);

	// Bottom-right vertex (corner)
	glTexCoord2i( 1, 1 );
	glVertex2f( m_cBox.x+m_cBox.w, m_cBox.y+m_cBox.h);


	// Top-right vertex (corner)
	glTexCoord2i( 0, 1 );
	glVertex2f( m_cBox.x+m_cBox.w, m_cBox.y);

	glEnd();
	}*/
	// fliped


	glEnable (GL_BLEND);

	if(m_GLTexture!=0)
	{

		glBindTexture( GL_TEXTURE_2D, m_GLTexture );
		float x =((float)m_cBox.w/2.0f);
		x*=-1;
		float y = ((float)m_cBox.h/2.0f);
		y*=-1;
		float s = ((float)m_cBox.w/(float)m_iTextureW);
		float t = ((float)m_cBox.h/(float)m_iTextureH);
		float w = (float)m_cBox.w;
		float h = (float)m_cBox.h;
		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		//
		glTranslatef(m_cBox.x-x,m_cBox.y-y,0);
		glRotatef((float)m_iRotation,0,0,1);
		glScalef((float)m_iScaleX,(float)m_iScaleY,1);

		glBegin( GL_QUADS );
		// Top-left vertex (corner)
		glTexCoord2f( 0,0);
		glVertex2f( x,y);


		// Bottom-left vertex (corner)
		glTexCoord2f( 0,t );
		glVertex2f( x, y+h);

		// Bottom-right vertex (corner)
		glTexCoord2f( s,t );
		glVertex2f( x+w, y+h);


		// Top-right vertex (corner)
		glTexCoord2f( s, 0 );
		glVertex2f( x+w, y);

		glEnd();


		glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glDisable(GL_BLEND);
		glPopMatrix();
	}

}

bool EEWidget::SetScalingFactor(double _ScalingX,double _ScalingY)
{
	if(m_iScaleX!=_ScalingX || m_iScaleY!=_ScalingY)
	{
		if(m_pModifySurface)
		{
			SDL_FreeSurface(m_pModifySurface);
			m_pModifySurface=NULL;
		}
	}
	m_iScaleX=_ScalingX;
	m_iScaleY=_ScalingY;


	if (m_iScaleX==_ScalingX && m_iScaleY==_ScalingY )
		return true;
	else
		return false;

}


bool EEWidget::SetScalingFactorY(double _ScalingY)
{
	if( m_iScaleY!=_ScalingY)
	{
		if(m_pModifySurface)
		{
			SDL_FreeSurface(m_pModifySurface);
			m_pModifySurface=NULL;
		}
	}
	m_iScaleY=_ScalingY;
	if (m_iScaleY==_ScalingY )
		return true;
	else
		return false;

}
bool EEWidget::SetScalingFactorX(double _ScalingX)
{
	if(m_iScaleX!=_ScalingX)
	{
		if(m_pModifySurface)
		{
			SDL_FreeSurface(m_pModifySurface);
			m_pModifySurface=NULL;
		}
	}
	m_iScaleX=_ScalingX;
	if (m_iScaleX==_ScalingX )
		return true;
	else
		return false;

}

double  EEWidget::GetScalingFactorX()
{
	return m_iScaleX;

}
double  EEWidget::GetScalingFactorY()
{
	return m_iScaleY;

}
bool EEWidget::SetRotation(double _Degrees )
{
	if(	m_iRotation!=_Degrees)
	{
		if(m_pModifySurface)
		{
			SDL_FreeSurface(m_pModifySurface);
			m_pModifySurface=NULL;
		}
	}
	m_iRotation=_Degrees;
	if (m_iRotation==_Degrees)
		return true;
	else
		return false;

}
double EEWidget::GetRotation()
{
	return m_iRotation;
}

bool EEWidget::SetAntiAliased(bool _AntiAliased )
{
	m_iAntiAliased=_AntiAliased;
	if (m_iAntiAliased==_AntiAliased)
		return true;
	else
		return false;

}
bool EEWidget::GetAntiAliased()
{
	return m_iAntiAliased;
}

int EEWidget::GetCurrentVectorPostion()
{
	return m_iCurrentPostion;
}

EEWidget * EEWidget::WasClicked(int _mouselocationX, int _mouselocationY)
{

	if(m_bactive )
	{


		if( ( _mouselocationX >  m_cBox.x ) && ( _mouselocationX <  m_cBox.x +  m_cBox.w ) && (_mouselocationY >  m_cBox.y ) && (_mouselocationY < m_cBox.y + m_cBox.h ) )
		{
			this->m_bCurrentlySelected=true;
			return this;
		}




	}

	this->m_bCurrentlySelected=false;
	return NULL;

}

void EEWidget::DisableClick()
{
	m_bactive=false;
}

void EEWidget::EnableClick()
{
	m_bactive=true;
}
bool EEWidget::GetIsClickEnabled()
{
	return m_bactive;
}

void EEWidget::DisableAndTurnOffDraw()
{
	m_bactive=false;
	SetDraw(false);
}

void EEWidget::EnableAndTurnOnDraw()
{
	m_bactive=true;
	SetDraw(true);
}


void EEWidget::SetDontMoveOnClick(bool _flag)
{
	m_bDontMoveButton=_flag;
}

bool EEWidget::GetDontMoveOnClick()
{
	return m_bDontMoveButton;
}




bool EEWidget::GetBlit()
{
	return this->m_bblit;
}

SDL_Surface * EEWidget::SetSurface()
{
	return NULL;
}

SDL_Surface * EEWidget::GetEEWidgeturface()
{
	return m_porignalSurface;
}


SDL_Rect * EEWidget::GetRect()
{
	return  &m_cBox;
}

bool EEWidget::SetCurrentArrayPostion(int _newPostion)
{
	m_iCurrentPostion=_newPostion;
	if(  m_iCurrentPostion==_newPostion)
	{
		return true;
	}
	else
		return false;
}

void EEWidget::SetType(int _type)
{
	m_iSprocketType=_type;
}
void EEWidget::SetLocationZ(int _z)//DO NOT call this function call ChangeSprocketZ instead
{
	m_iz=_z;
}

void EEWidget::SetBlit(bool _blit)
{
	if(_blit)
	{
		if(m_pModifySurface)
		{
			SDL_FreeSurface(m_pModifySurface);
		}
		m_pModifySurface=NULL;
	}
	m_bblit=_blit;
}

void EEWidget::SetModifySurFace(SDL_Surface * _ModifySurface)
{
	if(_ModifySurface)
	{
		if(m_pModifySurface)
		{
			SDL_FreeSurface(m_pModifySurface);
		}
		m_pModifySurface=_ModifySurface;
		SetHeightWidth(m_pModifySurface->h,m_pModifySurface->w);
	}

}

SDL_Surface *  EEWidget::GetModifySurFace()
{
	return 	m_pModifySurface;
}

void EEWidget::ShowOutLine()
{
	m_bShowSurfaceOutline=true;
}

void EEWidget::HideOutLine()
{
	m_bShowSurfaceOutline=false;
	SetBlit(true);
	if(m_pModifySurface)
	{
		SDL_FreeSurface(m_pModifySurface);
	}
	m_pModifySurface=NULL;


}

bool EEWidget::GetOutLine()
{
	return m_bShowSurfaceOutline;
}


//************************End Of Sprocket Class Funtions*****************
