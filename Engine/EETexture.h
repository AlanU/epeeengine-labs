/*
*  EETexture.h
*
*
*  Epee Engine
*  Created by Alan Uthoff on 3/12/06.
Copyright (C) 2011

This Code is free software; you can redistribute it and/or
modify it under the terms of the zlib/libpng License as published
by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This software is provided 'as-is', without any express or implied warranty.

In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.


*/


#ifndef EETEXTURE_H
#define EETEXTURE_H
#include "EpeeEngineCommenIncludes.h"
//***********************Begine Texture Class*******************************
class EETexture
{
public:
	GLuint m_GLTexture;
	int m_iTextureH;
	int m_iTextureW;
	int m_iClippingH;
	int m_iClippingW;
	EETexture()
	{
		m_GLTexture=0;
		m_iTextureH=0;
		m_iTextureW=0;
		m_iClippingH=0;
		m_iClippingW=0;
	}
	~EETexture()
	{
		if(m_GLTexture!=0)
		{
			glDeleteTextures( 1, &m_GLTexture);
		}
	}
	EETexture(const EETexture &_Copy)
	{
		m_GLTexture = _Copy.m_GLTexture;
		m_iTextureH = _Copy.m_iTextureH;
		m_iTextureW = _Copy.m_iTextureW;
		m_iClippingH = _Copy.m_iClippingH;
		m_iClippingW = _Copy.m_iClippingW;

	}
	virtual EETexture& operator=(const EETexture &_Copy)
	{
		m_GLTexture = _Copy.m_GLTexture;
		m_iTextureH = _Copy.m_iTextureH;
		m_iTextureW = _Copy.m_iTextureW;
		m_iClippingH = _Copy.m_iClippingH;
		m_iClippingW = _Copy.m_iClippingW;
		return *this;
	}
};


//***********************End Texture Class*********************************


#endif // EETEXTURE_H
