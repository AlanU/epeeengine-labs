/*
*  EESound.cpp
*
*
*  Epee Engine
*  Created by Alan Uthoff on 3/12/06.
Copyright (C) 2011

This Code is free software; you can redistribute it and/or
modify it under the terms of the zlib/libpng License as published
by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This software is provided 'as-is', without any express or implied warranty.

In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.


*/


#include "EESound.h"

//*************Sound Class Funtions*******************************
Sound::Sound(const std::string & _Name ,const std::string & _FileName,int _Channel,int _NumberOfTimesToLoop, int _Volume)
{
	m_sName=_Name;
	m_sFileName=_FileName;
	m_iChannel=_Channel;
	m_pSound=NULL;
	m_iNumberOfTimesToLoop=_NumberOfTimesToLoop;
	if (_Volume>=0)
	{
		m_iVolume=_Volume;
	}
	else
	{
		m_iVolume=MIX_MAX_VOLUME;
	}

}

Sound::~Sound()
{
	if (m_pSound) {
		Mix_FreeChunk(m_pSound);

	}
}

int Sound::GetVolume()
{

	return m_iVolume;
}
bool Sound::SetVolume(int _Volume)
{

	if (m_pSound)
	{
		m_iVolume=_Volume;
		Mix_Volume(m_iChannel,m_iVolume);
		return true;
	}
	else
	{
		return false;
	}
}
bool Sound::LoadSound()
{

	if (m_pSound)
	{
		Mix_FreeChunk(m_pSound);
	}

	m_pSound=Mix_LoadWAV(m_sFileName.c_str());

	if (m_pSound)
	{
		Mix_Volume(m_iChannel,m_iVolume);
		return true;
	}
	else
	{
		return false;
	}


}

Mix_Chunk * Sound::GetMixChunkPointer()
{
	return m_pSound;
}

bool Sound::SetChannel(int _ChannelToSetTo)
{
	m_iChannel=_ChannelToSetTo;
	if (m_iChannel==_ChannelToSetTo)
	{
		return true;
	}
	else
		return false;
}

int Sound::GetChannel()
{
	return m_iChannel;
}

bool Sound::SetNumberOfTimesToLoop(int _NumberOfTimesToLoop)
{
	m_iNumberOfTimesToLoop=_NumberOfTimesToLoop;
	if (m_iNumberOfTimesToLoop==_NumberOfTimesToLoop)
	{
		return true;
	}
	else
		return false;
}

int Sound::GetNumberOfTimesToLoop()
{
	return m_iNumberOfTimesToLoop;
}
std::string Sound::GetName()
{
	return m_sName;
}
//************************End Of Sound Class Funtions*****************************
