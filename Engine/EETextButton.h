/*
*  EETextButton.h
*
*
*  Epee Engine
*  Created by Alan Uthoff on 3/12/06.
Copyright (C) 2011

This Code is free software; you can redistribute it and/or
modify it under the terms of the zlib/libpng License as published
by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This software is provided 'as-is', without any express or implied warranty.

In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.


*/


#ifndef EETEXTBUTTON_H
#define EETEXTBUTTON_H
#include "EETextBox.h"
//********************************Begin of ButtonTB Class********************************
class ButtonTB :public textBox
{

	int m_ibuttonx;
	int m_ibuttony;

public:
	ButtonTB(const std::string & _ButtonName,int _Buttonx,int _Buttony,int _Buttonz,const std::string & _ButtonText ,bool _SButtonactive=true,unsigned int _ButtonTB_height=30,unsigned int _ButtonTB_width=100,const std::string & _ButtonTB_Font=True_Type_Font_Defalut,int _ButtonTB_FontPoint=18,unsigned int _ButtonTB_red=255,unsigned int _ButtonTB_blue=255 ,unsigned int _ButtonTB_green=255,const std::string & _ButtonFontPath=True_Type_Font_Location);//:textBox(Type_ButtonTB,_ButtonName, _Buttonx,_Buttony,_Buttonz,_ButtonText, _ButtonTB_height, _ButtonTB_width,_ButtonTB_Font,_ButtonTB_FontPoint,_ButtonTB_red, _ButtonTB_blue ,_ButtonTB_green,_ButtonFontPath)

};

//********************************End of ButtonTB Class********************************


#endif // EETEXTBUTTON_H
