/*
*  EEImage.h
*
*
*  Epee Engine
*  Created by Alan Uthoff on 3/12/06.
Copyright (C) 2011

This Code is free software; you can redistribute it and/or
modify it under the terms of the zlib/libpng License as published
by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This software is provided 'as-is', without any express or implied warranty.

In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.


*/

#ifndef EEIMAGE_H
#define EEIMAGE_H

#include "EpeeEngineCommenIncludes.h"
#include <string>
#include <iostream>
#include "EEWidget.h"

//***********************Begin of image Class****************************

class image :public EEWidget
{	SDL_Rect m_cOrgialDem;
std::string m_sFileName;
bool m_bClip;
bool m_bTransparency;

public:
	image (const std::string & _FileNameImage,const std::string & _NameImageHere,int _x,int _y,int _z,bool _Transparency=false);//:EEWidget( _NameImageHere,Type_Image ,_x, _y, _z)
	friend class EpeeEngine;
	virtual SDL_Surface * LoadSurface();
	virtual void LoadTexture();
	std::string GetFileName();
	void SetClippingRect(int _x,int _y,int _w,int _h);
	bool GetClip();
	int GetClippingX();
	int GetClippingY();
	int GetClippingW();
	int GetClippingH();
	void SetTransparency (bool _Transparency);
	bool GetTransparency();
protected:
	SDL_Rect ClipingRect;

	image (const std::string & _FileNameImage,const std::string & _NameImageHere,int _x,int _y,int _z,int _type,bool _Transparency=false);//:EEWidget( _NameImageHere,_type ,_x, _y, _z)
	image();
	image(const std::string & _FileNameImage);
	SDL_Rect * GetClippingRect();
	SDL_Rect * GetOrignalRect();




};

//***********************End of image Class*********************************
#endif // EEIMAGE_H
