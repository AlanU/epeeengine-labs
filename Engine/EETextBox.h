/*
*  EETextBox.h
*
*
*  Epee Engine
*  Created by Alan Uthoff on 3/12/06.
Copyright (C) 2011

This Code is free software; you can redistribute it and/or
modify it under the terms of the zlib/libpng License as published
by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This software is provided 'as-is', without any express or implied warranty.

In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.


*/

#ifndef EETEXTBOX_H
#define EETEXTBOX_H

#include "EpeeEngineCommenIncludes.h"
#include <iostream>
#include <sstream>
#include "EEWidget.h"
#include "EpeeUtility.h"
#include "EELog.h"
//***********************Begin of textBox Class****************************
class textBox :public EEWidget
{
private:
	std::string m_sTrue_Type_Font_Location;
	std::string m_sTextBoxMessage;
	std::string m_sFont;
	int m_iFontPoint;
	SDL_Color m_cColorOfText;
	SDL_Color m_cBackGroundColor;
	SDL_Color m_cColorOfSelectedText;
	SDL_Color m_cSelectedBackGroundColor;
	bool m_bblit;
	TTF_Font *m_pLoadedFont;
	bool m_bEditable;
	bool m_bNumbersOnly;
	int m_iJustify;
	int m_iOffsetXJustiy;
	bool m_bDoNotClipTextHeight;
	bool m_bDoNotClipTextWidth;
	int m_bQuality;
	bool m_bChangeTextBoxColorOnClick;
	SDL_Rect m_cModBox;

public:

protected:
	textBox(int _Obj_Type,const std::string & _NameTextBox,int _x,int _y,int _z,const std::string & _TextBoxMessage=" ",unsigned int _height=-1,unsigned int _width=-1,const std::string & _Font=True_Type_Font_Defalut,int _FontPoint=18,unsigned int _red=255,unsigned int _blue=255 ,unsigned int _green=255,const std::string & _FontPath = True_Type_Font_Location);//:EEWidget(_NameTextBox,_Obj_Type);
	virtual void SetModifySurFace(SDL_Surface * _ModifySurface);
	virtual void Render();
public:
	textBox(const std::string & _NameTextBox, int _x, int _y,int _z,const std::string & _TextBoxMessage=" ",unsigned int _height=-1,unsigned int _width=-1,const std::string & _Font=True_Type_Font_Defalut,int _FontPoint=18,unsigned int _red=255,unsigned int _blue=255 ,unsigned int _green=255,const std::string & _FontPath = True_Type_Font_Location);//:EEWidget(_NameTextBox,Type_TextBox);
	textBox();
	~textBox();

	friend class EpeeEngine;

	SDL_Surface * LoadSurface();
	bool GetClipTextHeight();
	bool GetClipTextWidth();
	void SetQuality(int _Quality);
	int GetQuality();
	void SetText(const std::string & _text,bool _setevent=false);
	virtual void SetHeightWidth(unsigned int _Height,unsigned int _Width);
	void SetHightBasedOfRenderedText();
	void SetWidthBasedOfRenderedText();
	int GetJustifyOffset();
	int GetTextJustification();
	void SetTextJustification(int _justify);
	std::string GetText ();
	bool GetTextToInt(int & _value);
	bool GetTextToFloat(float & _value);
	bool  SetTextFromInt(int _value,bool _setevent=false);
	bool SetTextFromFloat(float _value,bool _setevent=false);
	std::string GetFont();
	void SetFont(const std::string & _FontArg);
	int GetFontPoint();
	void SetFontPoint(int _FontPointArg);
	SDL_Color GetBackGroundColor();
	bool SetBackGroundColor(unsigned int _red,unsigned int _blue ,unsigned int _green);
	SDL_Color GetTextColor();
	bool SetTextColor(unsigned int _red,unsigned int _blue ,unsigned int _green);
	std::string GetFontPath();
	void SetFontPath(const std::string & _Location);
	void SetEditable(bool _flag);
	bool GetEditable();
	void SetNumbersOnly(bool _flag);
	bool GetNumbersOnly();
	bool IsColorChangedOnClick();
	void SetColorChangedOnClick(bool _ChangeOnClick);
	void SetSelectBackGroundColor(unsigned int _red,unsigned int _blue ,unsigned int _green);
	void SetSelectTextColor(unsigned int _red,unsigned int _blue ,unsigned int _green);
	virtual EEWidget * WasClicked(int _mouselocationX, int _mouselocationY);
	//	template <class datatype>
	//	datatype GetTextToNumericData(datatype _type);

	//	template <class datatype>
	//	bool  SetTextFromNumericData(datatype _value,bool _setevent);
	template <class datatype>
	bool GetTextToNumericData(datatype &_type)
	{
        if (!EpeeUtility::StringToNumericData(m_sTextBoxMessage,_type))
        {
            EE_WARNING<<"Could not convert \""+m_sTextBoxMessage+"\" to the specified data type"<<std::endl;
            return false;
        }
        return true;
	}

	template <class datatype>
	bool  SetTextFromNumericData(datatype _value,bool _setevent)
	{
		datatype tempint=_value;
        std::string tempstr= " ";
		if (!EpeeUtility::StringFromNumericData(tempstr, tempint))
        {

			EE_DEBUG<<"Could not convert numeric data "<<_value<<" to string";
			return false;

		}
		this->SetText(tempstr,_setevent);
		return true;
	}


	bool  WillTextFitIntoBox(std::string _text);
protected:
	TTF_Font * GetFontPointer();

	void LoadFont();
protected:
	void SetJustifyOffset();
};
//***********************End of textBox Class****************************

#endif // EETEXTBOX_H
