/*
*  EEAnimationScript.cpp
*
*
*  Epee Engine
*  Created by Alan Uthoff on 3/12/06.
Copyright (C) 2011

This Code is free software; you can redistribute it and/or
modify it under the terms of the zlib/libpng License as published
by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This software is provided 'as-is', without any express or implied warranty.

In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.


*/

#include "EEAnimationScript.h"
//************************Begin of Animatoion Script Class Funtions*******************






AnimationScript::AnimationScript(const std::string & _AnimationName,EEWidget * _AnimationSprocket,int _z,PlayScrtipFuntionPointer* _ScrtipFuntionPointer,int _fps,int _TotalNumberOfFrames,int _StartFrame,int _Loop,bool _StartNow):EEWidget(_AnimationName,Type_AnimationScript,0,0,_z)
{
	m_fPlayScrtipFuntionPointer=_ScrtipFuntionPointer;
	m_ianimationFPS=_fps;
	m_iFrameEverInMillSecounds=1000.0f/(float)m_ianimationFPS;
	m_bFuntionIsDoneWithOneCycle=false;
	m_pAnimationSprocket=_AnimationSprocket;

	if(_TotalNumberOfFrames==0 ||  _TotalNumberOfFrames<-1)
	{
		m_iNumFrames=1;
	}
	else
	{
		m_iNumFrames=_TotalNumberOfFrames;
	}

	if (_Loop>=0)
	{
		m_iLoop=_Loop;
	}
	else
	{
		m_iLoop=1;
	}
	m_iCurrentFrame=0;
	m_iPrevTime=0;
	m_iCurrentTime=(float)SDL_GetTicks();
	m_bPlaying=_StartNow;
	JumpToFrame(_StartFrame);
	m_iNumberofTimesPlayed=0;
	m_bReversPlay=false;
	m_iframeEvent=-1;

}

void  AnimationScript::JumpToFrame(int _Frame)
{
	int Currentframe=0;
	if(m_iCurrentFrame!=_Frame)
	{
		for (Currentframe=0 ; Currentframe<=_Frame; Currentframe++)
		{

			if(m_fPlayScrtipFuntionPointer)
			{
				m_bFuntionIsDoneWithOneCycle=(*m_fPlayScrtipFuntionPointer)(m_iCurrentTime, m_pAnimationSprocket);
			}
			else
			{
				m_bFuntionIsDoneWithOneCycle=Script(m_iCurrentTime, m_pAnimationSprocket);
			}
			Currentframe++;

		}

		m_iCurrentFrame=Currentframe;
	}

}

void AnimationScript::UpdateTime()
{

	float time=SDL_GetTicks()-m_iCurrentTime;
	CheckForNextFrame(time);
	//cout<<"Animation Upate Current Frame "<<m_iCurrentFrame<<" Time Diffrence "<<m_iCurrentTime-m_iPrevTime<<" frame pre millsecound "<<m_iFrameEverInMillSecounds<<std::endl;

	//call play script funtion


}

int AnimationScript::GetFPS()
{
	return m_ianimationFPS;
}

void AnimationScript::SetFPS(int _fps)
{
	m_ianimationFPS=_fps;
	m_iFrameEverInMillSecounds=1000.00f/(float)m_ianimationFPS;
	UpdateTime();

}



void AnimationScript::StopAnimation()
{
	m_bPlaying=false;
}

void AnimationScript::PlayAnimation()
{
	m_bPlaying=true;
	m_iNumberofTimesPlayed=0;

}

void AnimationScript::ResumeAnimation()
{
	if(m_iNumberofTimesPlayed<m_iLoop)
	{
		m_bPlaying=true;
	}

}

void AnimationScript::SetLoop(int _Loop)
{
	if (_Loop>=0)
	{
		m_iLoop=_Loop;
	}
	else
	{
		m_iLoop=1;
	}
}

int AnimationScript::GetLoop()
{
	return m_iLoop;
}

int AnimationScript::GetCurrentFrame()
{
	return m_iCurrentFrame;
}

int AnimationScript::GetTotalNumberOfFrames()
{
	return m_iNumFrames-1;
}

int AnimationScript::GetNumberOfTimesPlayed()
{
	return m_iNumberofTimesPlayed;
}

void AnimationScript::SetFrameEvent(int _frame)
{
	m_iframeEvent=_frame;
}

int AnimationScript::GetFrameEvent()
{
	return m_iframeEvent;
}

bool AnimationScript::IsPlaying()
{
	return m_bPlaying;
}

void AnimationScript::CheckForNextFrame(float _time)
{
	if (_time>=m_iFrameEverInMillSecounds)
	{
		m_iCurrentTime=(float)SDL_GetTicks();
		if ((((m_iCurrentFrame<this->GetTotalNumberOfFrames() || !m_bFuntionIsDoneWithOneCycle) && m_bReversPlay==false)|| ((m_iCurrentFrame-1)>=0 && m_bReversPlay==true))&& !m_bFuntionIsDoneWithOneCycle)
		{
			bool setevent=false;
			if (m_bPlaying)
			{
				if (m_bReversPlay)
				{
					m_iCurrentFrame--;

					if (m_iCurrentFrame<=m_iframeEvent && m_iframeEvent!=-1) {
						setevent=true;
					}
					else
					{
						setevent=false;
					}

				}
				else
				{
					m_iCurrentFrame++;
					if (m_iCurrentFrame>=m_iframeEvent && m_iframeEvent!=-1)
					{
						setevent=true;
					}
					else
					{
						setevent=false;
					}
				}
				if (setevent && m_bPlaying)
				{

					SDL_Event event;

					event.type = SDL_USEREVENT;
					event.user.code = ANIMATION_EVENT;
					event.user.data1 = this;
					event.user.data2 = 0;
					SDL_PushEvent(&event);
					m_iframeEvent=-1;

				}



			}

		}
		else
		{
			if (m_bPlaying)
			{
				m_iNumberofTimesPlayed++;
				m_bFuntionIsDoneWithOneCycle=false;
				if (m_iNumberofTimesPlayed>=m_iLoop && m_iLoop!=0)
				{
					StopAnimation();
				}
				else
				{

					if (m_bReversPlay)
					{
						m_iCurrentFrame=this->GetTotalNumberOfFrames();
					}
					else
					{

						m_iCurrentFrame=0;
					}

				}


			}

		}
		if(m_bPlaying)
		{
			if(m_fPlayScrtipFuntionPointer)
			{
				m_bFuntionIsDoneWithOneCycle=(*m_fPlayScrtipFuntionPointer)(m_iCurrentTime, m_pAnimationSprocket);
			}
			else
			{
				m_bFuntionIsDoneWithOneCycle=Script(m_iCurrentTime, m_pAnimationSprocket);
			}
		}

	}



}

void AnimationScript::SetFuntionPointer(PlayScrtipFuntionPointer * _PlayScrtipFuntionPointer)
{
	m_fPlayScrtipFuntionPointer=_PlayScrtipFuntionPointer;
}

PlayScrtipFuntionPointer  AnimationScript::GetFuntionPointer()
{
	return (*m_fPlayScrtipFuntionPointer);

}

bool AnimationScript::Script(float _time,EEWidget * _AnimationSprocket)
{
	return true;
}
// Script(float _time,EEWidget * _AnimationSprocket);

//************************End Of Animation Script Class Funtions**********************

