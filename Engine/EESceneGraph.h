/*
*  EESceneGraph.h
*
*
*  Epee Engine
*  Created by Alan Uthoff on 3/12/06.
Copyright (C) 2011

This Code is free software; you can redistribute it and/or
modify it under the terms of the zlib/libpng License as published
by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This software is provided 'as-is', without any express or implied warranty.

In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.


*/

#ifndef EESCENEGRAPH_H
#define EESCENEGRAPH_H
#include "EpeeEngineCommenIncludes.h"
#include <string>
#include <vector>
#include "EEWidget.h"
//***********************Begin Of RenderList Class***************************
class RenderList
{
private:
	std::string m_sNameOfRenenderList;
	std::vector  <EEWidget*> m_vEEWidgetList;
	EEWidget  * m_pLastSprocketFound;
	int m_uiTotalEEWidgetCreated;
public:

	RenderList(const std::string & _name);
	~RenderList();

	friend class EpeeEngine;
	std::string GetName();
	void LoadAllEEWidget();
	void UnloadAllEEWidget();
	bool InsertSprocket(EEWidget * _newSprocket,bool _new=true);
	bool DestroySprocket(const std::string & _name);
	bool UpdateVectorlocation();
	bool ChangeSprocketZ(EEWidget * _SprocketToChange,int _newZ);
	EEWidget *  FindEEWidget(const std::string & _name);
	unsigned int GetTotalSprockesCreated();
	void RemoveSprocketFromList(EEWidget * _sprocketToErase);
	void DrawAllEEWidget(bool _bToggle);

};
//***********************End Of RenderList Class***************************


#endif // EESCENEGRAPH_H
