/*
*  EESceneGraph.cpp
*
*
*  Epee Engine
*  Created by Alan Uthoff on 3/12/06.
Copyright (C) 2011

This Code is free software; you can redistribute it and/or
modify it under the terms of the zlib/libpng License as published
by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This software is provided 'as-is', without any express or implied warranty.

In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.


*/

#include "EESceneGraph.h"
#include "EELog.h"
#include <string>
#include <iostream>
//*************************Begining Of RenderList Class Funtions***********************************
RenderList::RenderList(const std::string & _name)
{
	m_sNameOfRenenderList=_name;
	m_pLastSprocketFound=NULL;
	m_uiTotalEEWidgetCreated=0;
}


RenderList::~RenderList()
{
	unsigned int TotalEEWidgetDeleted=0;

	while(m_vEEWidgetList.size()!=0)
	{
		DestroySprocket(m_vEEWidgetList[0]->GetName());
		TotalEEWidgetDeleted++;

	}

	if (m_uiTotalEEWidgetCreated!=0)
	{
		EE_ERROR<<"*****WARING MEMORY LEAK****"<<std::endl;
		EE_ERROR<<"There are still "<<m_uiTotalEEWidgetCreated<<" EEWidget that where not deleted"<<std::endl;
	}
}


std::string RenderList::GetName()
{
	return m_sNameOfRenenderList;
}


bool RenderList::InsertSprocket(EEWidget * _newSprocket,bool _new)
{
#if defined DEBUG_ME

	int tempz=0;
	if(m_vEEWidgetList.size()!=0)
		tempz=m_vEEWidgetList[m_vEEWidgetList.size()-1]->GetLocationZ();


#endif
	if (m_vEEWidgetList.size()!=0 && !(m_vEEWidgetList[m_vEEWidgetList.size()-1]->GetLocationZ() > _newSprocket->GetLocationZ()))
	{
		//m_vEEWidgetList.push_back(_newSprocket);
		for(unsigned int index2=0; index2<m_vEEWidgetList.size();index2++)
		{

			if(_newSprocket->GetLocationZ()>=m_vEEWidgetList[index2]->GetLocationZ())
			{
				m_vEEWidgetList.insert(m_vEEWidgetList.begin()+index2,_newSprocket);
				_newSprocket->SetCurrentArrayPostion(index2);
				if(_new)
				{
					m_uiTotalEEWidgetCreated++;
				}
				break;
			}

		}
	}
	else
	{
		m_vEEWidgetList.push_back(_newSprocket);
		if(_new)
		{
			m_uiTotalEEWidgetCreated++;
		}
	}

	return true;

}


bool RenderList::DestroySprocket(const std::string &_name)
{
	EEWidget * temp=NULL;
	bool retunval=false;
	for(unsigned int index = 0;index<m_vEEWidgetList.size();index++)
	{
		if(m_vEEWidgetList[index]->GetName()==_name)
		{
			if(m_pLastSprocketFound!=NULL)
			{
				if(m_pLastSprocketFound->GetName()==_name)
					m_pLastSprocketFound=NULL;
			}
			temp=m_vEEWidgetList[index];
			m_vEEWidgetList.erase(m_vEEWidgetList.begin()+index);
			delete temp;
			m_uiTotalEEWidgetCreated--;
			retunval=true;
			index=m_vEEWidgetList.size();
		}
	}
	if (!retunval)
	{
		EE_ERROR<<"Could Not Find And Delete Sprocket "<<_name<<" "<<std::endl;
	}
	return retunval;
}


bool RenderList::UpdateVectorlocation()
{
	for(unsigned int index = 0;index<m_vEEWidgetList.size();index++)
	{
		m_vEEWidgetList[index]->SetCurrentArrayPostion(index);
	}
	return true;
}


bool RenderList::ChangeSprocketZ(EEWidget * _SprocketToChange,int _newZ)
{
	UpdateVectorlocation();
	m_vEEWidgetList.erase(m_vEEWidgetList.begin()+_SprocketToChange-> GetCurrentVectorPostion());
	_SprocketToChange->SetLocationZ(_newZ);
	InsertSprocket(_SprocketToChange);
	return true;
}

void RenderList::DrawAllEEWidget(bool _bToggle)
{
	for (unsigned int index=0;index<m_vEEWidgetList.size();index++)
	{
		m_vEEWidgetList[index]->SetDraw(_bToggle);
	}
}
void RenderList::LoadAllEEWidget()
{
	for (unsigned int index=0;index<m_vEEWidgetList.size();index++)
	{
		if(!m_vEEWidgetList[index]->IsLoaded())
		{

			if(m_vEEWidgetList[index]->LoadSurface())
			{
				m_vEEWidgetList[index]->LoadTexture();
			}

		}
	}
}

void RenderList::UnloadAllEEWidget()
{
	for (unsigned int index=0;index<m_vEEWidgetList.size();index++)
	{
		if(m_vEEWidgetList[index]->IsLoaded())
		{
			m_vEEWidgetList[index]->UnloadSurface();
			m_vEEWidgetList[index]->UnLoadTexture();
		}
	}
}

EEWidget *  RenderList::FindEEWidget(const std::string & _name)
{
#if defined(DEBUG_ME)
	std::string temp_name=" ";
#endif
	if(m_pLastSprocketFound!=NULL)
	{
		if(this->m_pLastSprocketFound->GetName()==_name)
		{
			return m_pLastSprocketFound;
		}
	}
	for (unsigned int index=0;index<m_vEEWidgetList.size();index++)
	{
#if defined(DEBUG_ME)
		temp_name=m_vEEWidgetList[index]->GetName();
		if(temp_name==_name)
		{
			m_pLastSprocketFound=m_vEEWidgetList[index];
			return m_vEEWidgetList[index];
		}
#else
		if(m_vEEWidgetList[index]->GetName()==_name)
		{
			m_pLastSprocketFound=m_vEEWidgetList[index];
			return m_vEEWidgetList[index];
		}

#endif
	}

	EE_WARNING<<"Could Not Find Sprocket "<<_name<<std::endl;
	return NULL;
}

unsigned int RenderList::GetTotalSprockesCreated()
{
	return m_uiTotalEEWidgetCreated;
}

void RenderList::RemoveSprocketFromList(EEWidget * _sprocketToErase)
{
	for(unsigned int index = 0;index<m_vEEWidgetList.size();index++)
	{
		if(m_vEEWidgetList[index]->GetName()==_sprocketToErase->GetName())
		{
			m_vEEWidgetList.erase(m_vEEWidgetList.begin()+index);
			break;
		}
	}
	m_pLastSprocketFound=NULL;
	m_uiTotalEEWidgetCreated--;
}
//***************************End Of RenderList Class Funtions***********************************

